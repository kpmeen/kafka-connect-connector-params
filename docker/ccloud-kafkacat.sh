#!/bin/bash

topic=$1

CURR_DIR=$(pwd)
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Read and export variables from the .ccloud.conf file
echo "Setting necessary environment variables..."
set -a
. $SCRIPT_DIR/.ccloud.conf
set +a
# Set the correct URL for secured Schema Registry
export SCHEMA_REGISTRY_URL="https://${SCHEMA_REGISTRY_API_KEY}:${SCHEMA_REGISTRY_API_SECRET}@${SCHEMA_REGISTRY_HOST}"

echo "Reading topic $topic from $CC_BOOTSTRAP_SERVER"

docker run --rm edenhill/kafkacat:1.5.0 \
  -X security.protocol=SASL_SSL \
  -X sasl.mechanisms=PLAIN \
  -X api.version.request=true \
  -b $CC_BOOTSTRAP_SERVER \
  -X sasl.username=$CC_KAFKA_KEY \
  -X sasl.password=$CC_KAFKA_SECRET \
  -t $topic \
  -C -o beginning \
  -s avro \
  -r $SCHEMA_REGISTRY_URL | jq

#docker run --rm edenhill/kafkacat:1.5.0 \
#  -X security.protocol=SASL_SSL \
#  -X sasl.mechanisms=PLAIN \
#  -X api.version.request=true \
#  -b $CC_BOOTSTRAP_SERVER \
#  -X sasl.username=$CC_KAFKA_KEY \
#  -X sasl.password=$CC_KAFKA_SECRET \
#  -t $topic \
#  -C -o beginning | jq
