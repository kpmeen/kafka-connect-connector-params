#!/bin/bash

CURR_DIR=$(pwd)
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Read and export variables from the .ccloud.conf file
set -a
echo "Setting necessary environment variables..."
. $SCRIPT_DIR/.ccloud.conf
set +a
export SCHEMA_REGISTRY_URL="https://${SCHEMA_REGISTRY_HOST}"

echo "Setting connector config properties from environment..."
INPUT_FILE=$SCRIPT_DIR/connector_configs/register-sqlserver-connector-template.json
OUTPUT_FILE=$SCRIPT_DIR/connector_configs/register-sqlserver-connector.json

escaped_registry_url="https:\/\/${SCHEMA_REGISTRY_HOST}"

bootstrap_regex="s/<cc_bootstrap_server>/$CC_BOOTSTRAP_SERVER/g"
kafka_key_regex="s/<cc_kafka_key>/$CC_KAFKA_KEY/g"
kafka_secret_regex="s/<cc_kafka_secret>/$CC_KAFKA_SECRET/g"
schema_registry_url_regex="s/<schema_registry_url>/$escaped_registry_url/g"
schema_registry_key_regex="s/<schema_registry_api_key>/$SCHEMA_REGISTRY_API_KEY/g"
schema_registry_secret_regex="s/<schema_registry_api_secret>/$SCHEMA_REGISTRY_API_SECRET/g"

sed -e $bootstrap_regex \
    -e $kafka_key_regex \
    -e $kafka_secret_regex \
    -e $schema_registry_url_regex \
    -e $schema_registry_key_regex \
    -e $schema_registry_secret_regex \
    $INPUT_FILE > $OUTPUT_FILE

echo "Deploying Debezium SQLServer connector..."
curl -X POST -H "Content-Type: application/json" --data @connector_configs/register-sqlserver-connector.json http://localhost:8083/connectors

#rm $OUTPUT_FILE
