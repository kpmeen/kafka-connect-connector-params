#!/bin/bash

echo "Waiting a little bit to ensure the DB has started properly"
sleep 15

while true
do
  lsof -i :1433 | grep LISTEN > /dev/null
  verifier=$?
    if [ 0 = $verifier ]; then
      sqlcmd -S localhost -U SA -P Alaska2017 -Q "select * from sys.databases" > /dev/null
      status=$?
      if [ 0 = $status ]; then
        echo "Database is ready."
        break;
      else
        echo "Could not connect to DB. Exit code was $status"
      fi
    else
      echo "Waiting for DB to start..."
      sleep 5
    fi
done

echo "Preparing database and initialising CDC..."
sqlcmd -S localhost -U SA -P Alaska2017 -i /var/opt/db_init_scripts/prepare_db.sql

echo "DB preparation completed."