-------------------------------------------------------------------------------
-- Restore database from backup file..
RESTORE FILELISTONLY FROM DISK = N'/var/opt/mssql/backup/AdventureWorksDW2017.bak';
GO
RESTORE DATABASE AdventureWorksDW2017 FROM DISK = N'/var/opt/mssql/backup/AdventureWorksDW2017.bak' WITH MOVE 'AdventureWorksDW2017' TO '/var/opt/mssql/data/AdventureWorksDW2017.mdf', MOVE 'AdventureWorksDW2017_log' TO '/var/opt/mssql/data/AdventureWorksDW2017_log.ldf';
GO
SELECT * FROM AdventureWorksDW2017.dbo.DimCurrency;
GO

-------------------------------------------------------------------------------
-- Prepare users...
USE [master]
GO
CREATE LOGIN [cdc_user] WITH PASSWORD = 'cdc_user123';
GO
CREATE LOGIN [jimbob] WITH PASSWORD = 'hillbilly_123';
GO
USE [AdventureWorksDW2017]
GO
EXEC sp_changedbowner 'jimbob';
GO
CREATE USER [cdc_user] FOR LOGIN [cdc_user];
GO

-- Configure roles...
CREATE ROLE [cdc_owner];
ALTER ROLE [cdc_owner] ADD MEMBER [cdc_user];
ALTER ROLE [db_owner] ADD MEMBER [cdc_owner];
GO

-------------------------------------------------------------------------------
-- Enable CDC on database...
EXEC sys.sp_cdc_enable_db
GO

-- Enable CDC on tables...
EXEC sys.sp_cdc_enable_table
  @source_schema = N'dbo',
  @source_name   = N'DimEmployee',
  @role_name     = N'cdc_owner',
  @filegroup_name = N'PRIMARY',
  @supports_net_changes = 1
GO

EXEC sys.sp_cdc_enable_table
  @source_schema = N'dbo',
  @source_name   = N'DimCurrency',
  @role_name     = N'cdc_owner',
  @filegroup_name = N'PRIMARY',
  @supports_net_changes = 1
GO

EXEC sys.sp_cdc_help_change_data_capture
GO

-- select * from sys.database_principals;
-- GO