package net.scalytica.connect

import java.util.{Map => JMap, List => JList}
import scala.collection.mutable.{Map => MutableMap}
import scala.collection.JavaConverters._

package object transforms {

  implicit def jmapToMutableMap[K, V](jm: JMap[K, V]): MutableMap[K, V] =
    Option(jm).map(_.asScala).getOrElse(MutableMap.empty[K, V])

  implicit def jlistToList[T](jl: JList[T]): List[T] =
    Option(jl).map(_.asScala.toList).getOrElse(List.empty[T])
}
