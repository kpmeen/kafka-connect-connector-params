package net.scalytica.connect.transforms

import java.util.{Map => JMap}

import com.typesafe.scalalogging.Logger
import org.apache.kafka.common.cache.{Cache, LRUCache, SynchronizedCache}
import org.apache.kafka.common.config.ConfigDef.Validator
import org.apache.kafka.common.config.{ConfigDef, ConfigException}
import org.apache.kafka.connect.connector.ConnectRecord
import org.apache.kafka.connect.data._
import org.apache.kafka.connect.errors.DataException
import org.apache.kafka.connect.transforms.Transformation
import org.apache.kafka.connect.transforms.util.Requirements._
import org.apache.kafka.connect.transforms.util.SchemaUtil.copySchemaBasics
import org.apache.kafka.connect.transforms.util.SimpleConfig

object InjectConnectParam {

  private val CacheMaxSize: Int = 16
  private val NullValue         = null // scalastyle:ignore

  private[transforms] val NullValueErrMsg = "Record value cannot be null."
  private[transforms] val NullValueError  = new DataException(NullValueErrMsg)

  private[transforms] val Purpose =
    "Adding connect.params key-value to an existing field."

  private[transforms] object Keys {
    val ConnectParamsCfgKey    = "connect.parameters"
    val InjectIntoFieldCfgKey  = "inject.param.into"
    val InjectFieldCfgKey      = "inject.param.key"
    val InjectFieldValueCfgKey = "inject.param.value"
  }

  private[transforms] object OptionalValidator extends Validator {

    override def ensureValid(name: String, value: Any): Unit = {
      // it's OK to not do anything...
    }
  }

  private[transforms] val Config = new ConfigDef()
    .define(
      Keys.InjectIntoFieldCfgKey,
      ConfigDef.Type.STRING,
      ConfigDef.NO_DEFAULT_VALUE,
      ConfigDef.Importance.HIGH,
      "The field name to add connect.parameters into."
    )
    .define(
      Keys.InjectFieldCfgKey,
      ConfigDef.Type.STRING,
      ConfigDef.NO_DEFAULT_VALUE,
      ConfigDef.Importance.HIGH,
      "Name of field to add to connect.parameters."
    )
    .define(
      Keys.InjectFieldValueCfgKey,
      ConfigDef.Type.STRING,
      ConfigDef.NO_DEFAULT_VALUE,
      OptionalValidator,
      ConfigDef.Importance.HIGH,
      "String value to assign to the added field."
    )

  class Value[R <: ConnectRecord[R]] extends InjectConnectParam[R] {

    override protected def operatingSchema(record: R): Option[Schema] =
      Option(record.valueSchema)

    override protected def operatingValue(record: R): Option[Any] =
      Option(record.value)

    override protected def newRecordValue(
        record: R,
        updatedSchema: Schema,
        updatedValue: Any
    ): R =
      record.newRecord(
        record.topic,
        record.kafkaPartition,
        record.keySchema,
        record.key,
        updatedSchema,
        updatedValue,
        record.timestamp
      )
  }

  case class Cfg(
      intoField: String,
      paramField: String,
      paramFieldValue: String
  )
}

abstract class InjectConnectParam[R <: ConnectRecord[R]]
    extends Transformation[R] {

  import InjectConnectParam.Cfg
  import InjectConnectParam.Keys._

  private val logger = Logger(getClass)

  private var cfg: Cfg = _

  private var schemaUpdateCache: Cache[Schema, Schema] = _

  private def getConfigStringOpt(key: String)(implicit
      c: Option[SimpleConfig]
  ): Option[String] = c.flatMap(sc => Option(sc.getString(key)))

  private def getConfigString(
      key: String
  )(implicit c: Option[SimpleConfig]): String = {
    getConfigStringOpt(key).getOrElse {
      throw new ConfigException("Configuration is not yet initialized.")
    }
  }

  override def configure(props: JMap[String, _]): Unit = {
    implicit val sc = Option(new SimpleConfig(InjectConnectParam.Config, props))
    schemaUpdateCache = new SynchronizedCache[Schema, Schema](
      new LRUCache[Schema, Schema](InjectConnectParam.CacheMaxSize)
    )
    cfg = Cfg(
      intoField = getConfigString(InjectIntoFieldCfgKey),
      paramField = getConfigString(InjectFieldCfgKey),
      paramFieldValue = getConfigString(InjectFieldValueCfgKey)
    )
  }

  override def apply(record: R): R =
    operatingSchema(record).fold(record) { os =>
      transformWithSchema(os, record)
    }

  private def transformWithSchema(opSchema: Schema, record: R): R = {
    requireSchema(record.valueSchema(), InjectConnectParam.Purpose)

    logger.debug(s"Processing record $record")
    logger.trace(
      s"Record value schema fields are:\n${opSchema.fields.mkString("\n")}"
    )

    operatingValue(record)
      .map { ov =>
        logger.trace(s"Processing operating value $ov")
        val ovStruct = requireStruct(ov, InjectConnectParam.Purpose)

        val updSchema = Option(schemaUpdateCache.get(opSchema)).getOrElse {
          logger.trace("Schema not in cache. Building from value.schema...")
          val newSchema = buildUpdatedSchema(
            opSchema.schema,
            cfg.intoField,
            cfg.paramField,
            cfg.paramFieldValue
          )
          schemaUpdateCache.put(opSchema, newSchema)
          logger.trace("New schema was cached.")
          newSchema
        }

        val struct = new Struct(updSchema)
        opSchema.fields().foreach { f =>
          struct.put(f.name(), ovStruct.get(f.name()))
        }

        newRecordValue(record, updSchema, struct)
      }
      .getOrElse {
        logger.error(InjectConnectParam.NullValueErrMsg)
        throw InjectConnectParam.NullValueError
      }
  }

  private def buildUpdatedSchema(
      opSchema: Schema,
      into: String,
      key: String,
      value: String
  ) = {
    val builder = copySchemaBasics(opSchema)
    opSchema.fields.foreach { f =>
      if (f.name.equals(into)) {
        val ofs = f.schema()
        logger.debug(s"Field ${f.name} matched $InjectIntoFieldCfgKey=$into")
        logger.trace(s"Schema for $InjectIntoFieldCfgKey is $ofs")
        logger.trace(s"Operating params:\n${ofs.parameters.mkString("\n")}")

        val ufs = copySchemaBasics(ofs).parameter(key, value).build()

        logger.trace(s"Updated params:\n${ufs.parameters.mkString("\n")}")

        builder.field(f.name, ufs)
      } else {
        builder.field(f.name, f.schema)
      }
    }
    builder.build
  }

  override def config: ConfigDef = InjectConnectParam.Config

  override def close(): Unit = {
    schemaUpdateCache = InjectConnectParam.NullValue
  }

  protected def operatingSchema(record: R): Option[Schema]

  protected def operatingValue(record: R): Option[Any]

  protected def newRecordValue(
      record: R,
      updatedSchema: Schema,
      updatedValue: Any
  ): R

}
