package net.scalytica.connect.transforms

import java.util.UUID

import net.scalytica.connect.transforms.InjectConnectParam.Keys._
import net.scalytica.connect.transforms.InjectConnectParam.Value
import org.apache.kafka.connect.data._
import org.apache.kafka.connect.source.SourceRecord
import org.apache.kafka.connect.transforms.util.Requirements.requireStruct

import scala.collection.JavaConverters._
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

class InjectConnectParamSpec extends AnyWordSpec with Matchers {

  val purpose: String = null // scalastyle:ignore
  val testUuidString  = UUID.randomUUID().toString
  val testName        = "Darth Vader"
  val testAge         = 42

  val testSchema: Schema = SchemaBuilder.struct
    .name("name")
    .version(1)
    .doc("doc")
    .field("id", Schema.STRING_SCHEMA)
    .field("name", Schema.OPTIONAL_STRING_SCHEMA)
    .field("age", Schema.OPTIONAL_INT32_SCHEMA)
    .build

  val testSchemaWithParams: Schema = SchemaBuilder.struct
    .name("name")
    .version(1)
    .doc("doc")
    .field("id", SchemaBuilder.string().parameter("hello", "world").build())
    .field("name", Schema.OPTIONAL_STRING_SCHEMA)
    .field("age", Schema.OPTIONAL_INT32_SCHEMA)
    .build

  def testStruct(schema: Option[Schema] = None): Struct =
    schema
      .map(s => new Struct(s))
      .getOrElse(new Struct(testSchema))
      .put("id", testUuidString)
      .put("name", testName)
      .put("age", testAge)

  private def mockRecord(
      withSchema: Boolean = true,
      withParams: Boolean = false
  ): SourceRecord = {
    // scalastyle:off
    val schema = if (withSchema) {
      Option(if (withParams) testSchemaWithParams else testSchema)
    } else None
    new SourceRecord(null, null, "test", 0, schema.orNull, testStruct(schema))
    // scalastyle:on
  }

  "The transformer" when {

    "there is no schema" should {

      "do nothing if there is no schema" in {
        val transform = new Value[SourceRecord]
        transform.configure(
          Map(
            InjectIntoFieldCfgKey  -> "id",
            InjectFieldCfgKey      -> "uuid",
            InjectFieldValueCfgKey -> testUuidString
          ).asJava
        )
        val orig     = mockRecord(withSchema = false)
        val expected = requireStruct(orig.value, purpose)

        val res    = transform.apply(orig)
        val actual = requireStruct(res.value, purpose)

        actual mustBe expected
      }
    }

    "there is a schema" should {

      "add connect.parameters with a static value to a field" in {
        val localUuid = UUID.randomUUID().toString
        val transform = new Value[SourceRecord]
        transform.configure(
          Map(
            InjectIntoFieldCfgKey  -> "id",
            InjectFieldCfgKey      -> "uuid",
            InjectFieldValueCfgKey -> localUuid
          ).asJava
        )
        val orig     = mockRecord()
        val expected = requireStruct(orig.value, purpose)
        val transRec = transform.apply(orig)
        val schema   = transRec.valueSchema
        val actual   = requireStruct(transRec.value, purpose)

        val idSchema = schema.field("id").schema()

        actual.getString("id") mustBe testUuidString
        idSchema.parameters().asScala must contain("uuid" -> localUuid)
        actual.getString("id") mustBe expected.getString("id")
        actual.getString("name") mustBe expected.getString("name")
        actual.getInt32("age") mustBe expected.getInt32("age")
      }

      "append a key/value to a fields's connect.parameters" in {
        val transform = new Value[SourceRecord]
        transform.configure(
          Map(
            InjectIntoFieldCfgKey  -> "id",
            InjectFieldCfgKey      -> "uuid",
            InjectFieldValueCfgKey -> "true"
          ).asJava
        )
        val orig     = mockRecord(withParams = true)
        val expected = requireStruct(orig.value, purpose)
        val transRec = transform.apply(orig)
        val actual   = requireStruct(transRec.value, purpose)
        val schema   = transRec.valueSchema()

        val idSchema = schema.field("id").schema()

        actual.getString("id") mustBe testUuidString
        idSchema.parameters().asScala must contain("uuid"  -> "true")
        idSchema.parameters().asScala must contain("hello" -> "world")
        actual.getString("id") mustBe expected.getString("id")
        actual.getString("name") mustBe expected.getString("name")
        actual.getInt32("age") mustBe expected.getInt32("age")

//        val ac         = new AvroData(2)
//        val avroSchema = ac.fromConnectSchema(schema)
//        println(avroSchema.toString(true))
//
//        val serialized = ac.fromConnectData(schema, actual)
//        val des        = ac.toConnectData(avroSchema, serialized)
//
//        println(des)
      }

    }

  }

}
