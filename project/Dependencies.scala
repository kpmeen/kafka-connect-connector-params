import sbt._

object Versions {
  // TODO:
  // Cannot upgrade to Scala 2.13 yet, because some libs are only compiled
  // against 2.12.x.
  val ScalaVersion = "2.12.10"

  val ConfluentPlatformVersion = "5.5.0"
  val KafkaVersion             = "2.5.0"

  val CirceVersion              = "0.12.3"
  val CirceGenericExtrasVersion = "0.12.2"
  val CirceOpticsVersion        = "0.12.0"

  val ScalaLoggingVersion = "3.9.2"
  val Slf4JVersion        = "1.7.30"
  val LogbackVersion      = "1.2.3"

  val ScalaTestVersion = "3.1.2"
}

object Dependencies {
  // scalastyle:off

  import Versions._

  val Resolvers: Seq[Resolver] =
    DefaultOptions.resolvers(snapshot = true) ++ Seq(
      Resolver.typesafeRepo("releases"),
      Resolver.jcenterRepo,
      MavenRepo("confluent", "https://packages.confluent.io/maven/"),
      Resolver.bintrayRepo("hseeberger", "maven")
    )

  object Kafka {
    // official kafka libs
    val ConnectApi        = "org.apache.kafka" % "connect-api"                  % KafkaVersion             % Provided
    val AvroConverter     = "io.confluent"     % "kafka-connect-avro-converter" % ConfluentPlatformVersion % Provided
    val ConnectJson       = "org.apache.kafka" % "connect-json"                 % KafkaVersion             % Provided
    val ConnectTransforms = "org.apache.kafka" % "connect-transforms"           % KafkaVersion             % Provided
  }

  object Circe {
    val Optics = "io.circe" %% "circe-optics"         % CirceOpticsVersion
    val Extras = "io.circe" %% "circe-generic-extras" % CirceGenericExtrasVersion

    val All = Seq(
      "io.circe" %% "circe-core",
      "io.circe" %% "circe-generic",
      "io.circe" %% "circe-parser"
    ).map(_ % CirceVersion) :+ Optics :+ Extras
  }

  object Testing {
    val ScalaTest       = "org.scalatest" %% "scalatest" % ScalaTestVersion
    val Scalactic       = "org.scalactic" %% "scalactic" % ScalaTestVersion
    val JacksonDatabind = "com.fasterxml.jackson.core" % "jackson-databind" % "2.11.0"
    val ScalaTestDeps   = Seq(ScalaTest % Test, Scalactic)
  }

  object Logging {
    val ScalaLogging   = "com.typesafe.scala-logging" %% "scala-logging"   % ScalaLoggingVersion
    val Logback        = "ch.qos.logback"             % "logback-classic"  % LogbackVersion
    val Slf4j          = "org.slf4j"                  % "slf4j-api"        % Slf4JVersion
    val Log4jOverSlf4j = "org.slf4j"                  % "log4j-over-slf4j" % Slf4JVersion
    val Slf4jLog4j     = "org.slf4j"                  % "slf4j-log4j12"    % Slf4JVersion
    val JulToSlf4j     = "org.slf4j"                  % "jul-to-slf4j"     % Slf4JVersion
    val Slf4jNop       = "org.slf4j"                  % "slf4j-nop"        % Slf4JVersion

    val All = Seq(ScalaLogging, Slf4j, Logback)
  }
}
