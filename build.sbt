import Dependencies._
import Settings._
import net.scalytica.sbt.confluenthub.ConfluentHubKeys._
import net.scalytica.sbt.confluenthub._
import net.scalytica.sbt.plugin.DockerTasksPlugin
import sbtrelease.ReleaseStateTransformations._

import scala.language.postfixOps

// scalastyle:off

name := "kafka-connect-connector-params"

releaseProcess := Seq[ReleaseStep](
  checkSnapshotDependencies, // : ReleaseStep
  inquireVersions,           // : ReleaseStep
  runClean,                  // : ReleaseStep
  runTest,                   // : ReleaseStep
  setReleaseVersion,         // : ReleaseStep
  commitReleaseVersion,      // : ReleaseStep, performs the initial git checks
  tagRelease,                // : ReleaseStep
  setNextVersion,    // : ReleaseStep
  commitNextVersion, // : ReleaseStep
  pushChanges        // : ReleaseStep, also checks that an upstream branch is properly configured
)

lazy val assemblyCopyToDocker =
  taskKey[Unit]("Runs assembly and copies file to docker/custom_connectors")

lazy val root = (project in file("."))
  .enablePlugins(DockerTasksPlugin, ConfluentHubPlugin)
  .settings(
    // ConfluentHubPlugin settings
    hubOwnerName := developers.value.head.name,
    hubOwnerUrl := developers.value.headOption.map(_.url.toString),
    hubOwnerUsername := "kpmeen",
    hubComponentTypes := Seq(TransformComponent),
    hubArchiveDocFiles := Seq(
      file("./LICENSE"),
      file("./README.md")
    )
  )
  .settings(BaseSettings: _*)
  .settings(PublishSettings: _*)
  .settings(resolvers ++= Dependencies.Resolvers)
  .settings(scalastyleFailOnWarning := true)
  .settings(
    // Kafka/Confluent dependencies
    libraryDependencies ++= Seq(
      Kafka.ConnectApi,
      Kafka.ConnectJson,
      Kafka.AvroConverter,
      Kafka.ConnectTransforms
    )
  )
  .settings(
    // Logging dependencies
    libraryDependencies ++= Logging.All
  )
  .settings(
    // Test dependencies
    libraryDependencies ++= Seq(
      Logging.Log4jOverSlf4j  % Test,
      Logging.JulToSlf4j      % Test,
      Testing.ScalaTest       % Test,
      Testing.Scalactic       % Test,
      Testing.JacksonDatabind % Test
    )
  )
  .settings(
    assemblyJarName in assembly := s"${name.value}-${version.value}.jar"
  )
  .settings(
    // Task for assembling the artifact and copying it into the proper
    // sub-directory for the docker testing setup.
    assemblyCopyToDocker := {
      val dockerDest   = s"./docker/custom_connectors/${name.value}"
      val log          = streams.value.log
      val artifactPath = assembly.value
      val artifactName = (assemblyJarName in assembly).value

      log.info(s"Copying $artifactPath to $dockerDest ...")

      IO.copy(
        sources = Map(artifactPath -> file(dockerDest) / artifactName),
        overwrite = true,
        preserveLastModified = true,
        preserveExecutable = true
      )

      log.info(s"$artifactName copied to $dockerDest")
    }
  )
  .settings(
    // Disable adding scala version suffix to artifacts
    crossPaths := false
  )
  .settings(
    // Publishing of the fat jar
    packageBin in Compile := (assembly in Compile).value
  )
