# kafka-connect-connector-params

Kafka Connect SMT for adding/appending a key/value pair to the schema definition
for a given field under `connector.parameters` defined in its `type` definition.

#### Why?

There are use-cases where some simple flags can help with the understanding of
a fields _true_ meaning. UUIDs is one example, and is what actually triggered
the creation of this plugin.
In Kafka Connect UUID fields are currently given the type `string`, without any
extra information that will indicate the presence of an UUID value. By injecting
a key to `connector.parameters` as follows, we can derive from the schema that
the type should be interpreted as an UUID.

```
{
  "connect.name": "server1.schema.MyTable.Value",
  "fields": [
    {
      "name": "SomeUuid",
      "type": {
        "connect.parameters": {
          "uuid": "true"
        },
        "type": "string"
      }
    },
    // ...
  ],
  "name": "Value",
  "namespace": "server1.schema.MyTable",
  "type": "record"
}
```
